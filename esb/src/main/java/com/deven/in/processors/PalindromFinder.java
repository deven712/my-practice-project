package com.deven.in.processors;

import lombok.Value;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class PalindromFinder {
    private List<String> palindroms;
    public AtomicInteger locks = new AtomicInteger();
    public List<String> getPalindroms() {
        return palindroms;
    }

    public synchronized void addToPalindroms(String s){
        if(palindroms == null) palindroms =  new ArrayList<>();
        this.palindroms.add(s);
    }
    public void findPalindroms(String input, boolean caseSensitive){
        if (!caseSensitive) input= input.toUpperCase();
        char[] inputChars = input.toCharArray();

        for(int i=0;i<input.length();i++){
            if(i!= input.length()-1 && inputChars[i]==inputChars[i+1]) {
                locks.getAndIncrement();
                new PalindromThread(inputChars,i,true,this ).start();
            }
            if (i!=0 && i!= input.length()-1 && inputChars[i-1]==inputChars[i+1]) {
                locks.getAndIncrement();
                new PalindromThread(inputChars,i,false,this ).start();
            }
        }
    }
}

@Value
class PalindromThread extends Thread{
    private char[] baseString;
    private int initialIndex;
    private boolean isEvenLength;
    private PalindromFinder pf;

    @Override
    public void run(){
        LinkedList<Character> palString = new LinkedList<>();
        char[] palChars ;
        StringBuilder sb= new StringBuilder();
        int index =0;
        if(isEvenLength){
            try{while(baseString[initialIndex-index] == baseString[initialIndex+1+index]){
//                palString.addFirst(baseString[initialIndex-index]);
//                palString.addLast(baseString[initialIndex+1+index]);
                index++;
            }}
            catch(ArrayIndexOutOfBoundsException e){}
            --index;
            //System.out.println("Even initialIndex = " + initialIndex+ "index ="+index);
            palChars = Arrays.copyOfRange(baseString,initialIndex-index,initialIndex+1+index+1);
        }else{
            palString.add(baseString[initialIndex]);
            try{while(baseString[initialIndex-index-1] == baseString[initialIndex+1+index]){
//                palString.addFirst(baseString[initialIndex-index-1]);
//                palString.addLast(baseString[initialIndex+1+index]);
                index++;
            }}catch(ArrayIndexOutOfBoundsException e){}
            --index;
            //System.out.println("Odd initialIndex = " + initialIndex+ "index ="+index);
            palChars = Arrays.copyOfRange(baseString,initialIndex-index-1,initialIndex+1+index+1);
        }
        String finalPalindrom= new String(palChars);
        pf.addToPalindroms(finalPalindrom);
        pf.locks.getAndDecrement();
    }
}

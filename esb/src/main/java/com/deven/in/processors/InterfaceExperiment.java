package com.deven.in.processors;

public class InterfaceExperiment implements IntfA,IntfB{
    static Integer i;

    private static synchronized void staticMethod(){

    }
    public static synchronized void staticMethod2(){

    }

    @Override
    public int methodA(String s) {
        return 0;
    }

    @Override
    public Integer defaultMethodC(String s) {
        IntfA.super.defaultMethodC("Hello");
        IntfB.super.defaultMethodC("Hi");
        return null;
    }
}

interface IntfA{
    public int methodA(String s);
    public static int staticMethodB(String s){
        System.out.println("In IntfA Static method");
        return 0;
    }
    public default Integer defaultMethodC(String s){
        System.out.println("In IntfA default method");
        return null;
    }
}
interface IntfB{
    public int methodA(String s);
    public static int staticMethodB(String s){
        System.out.println("In IntfB Static method");
        return 0;
    }
    public default Integer defaultMethodC(String s){
        System.out.println("In IntfB default method");
        return null;
    }
}

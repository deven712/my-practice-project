package com.deven.in.processors;
/*
 * Enter your code here. Read input from STDIN. Print your output to STDOUT.
 * Your class should be named CandidateCode.
 */

import java.io.*;
import java.util.*;
import java.util.stream.LongStream;

public class CandidateCodeA {
    public static void main(String args[] ) throws Exception {
        Scanner scanner = new Scanner(System.in);
        Long noOfIngredient = Long.parseLong(scanner.nextLine());
        String requiredQunt = scanner.nextLine();
        String presentQuant = scanner.nextLine();
        scanner.close();
        LongStream requiredQuantities = Arrays.stream(requiredQunt.split(" ")).mapToLong(s -> Long.parseLong(s));
        LongStream presentQuantities= Arrays.stream(presentQuant.split(" ")).mapToLong(s -> Long.parseLong(s));
        Iterator<Long> rqIterator = requiredQuantities.iterator();
        Iterator<Long> pqIterator = presentQuantities.iterator();
        OptionalLong ppgCreated =LongStream.
                generate(() ->{
                            return (pqIterator.hasNext()&&rqIterator.hasNext())?
                                    Long.divideUnsigned(pqIterator.next(),rqIterator.next()):0;
                        } ).limit(noOfIngredient)
                .min();
        System.out.println(ppgCreated.orElse(0));
        //Write code here

    }
}

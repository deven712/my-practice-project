package com.deven.in.processors;

import org.apache.el.stream.Stream;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;



class Result {

    /*
     * Complete the 'longestVowelSubsequence' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts STRING s as parameter.
     */

    private static final List<Character> vovels = Arrays.asList('a','e','i','o','u','z');
    public static int longestVowelSubsequence(String s) {
        if (isValidSubsequance(s)) {
            int firstA = s.indexOf('a');
            int lastU = s.lastIndexOf('u');
            StringBuffer sequence = getLargestVovelSubsequence(s.toCharArray(), firstA, new StringBuffer());
            if (isValidSubsequance(s)) {
                System.out.println("Largest sequence = " + sequence);
                return sequence.length();
            }else return 0;
        } else return 0;
    }

    private static StringBuffer getLargestVovelSubsequence(char[] s, int index, StringBuffer stringBuffer) {
        if(stringBuffer.length()==0)   return getLargestVovelSubsequence(s,index+1,stringBuffer.append(s[index]));
        else if(s.length==index) {
            if(stringBuffer.charAt(stringBuffer.length()-1)=='u') {
                System.out.println(stringBuffer);
                return new StringBuffer(stringBuffer);
            }
            else {
                System.out.println("invalid sequence = " + stringBuffer);
                return new StringBuffer();
            }
        }
        else if(stringBuffer.charAt(stringBuffer.length()-1)==s[index]){
            return getLargestVovelSubsequence(s,index+1, stringBuffer.append(s[index]));
        }
        else if (s[index]==nextChar(stringBuffer.charAt(stringBuffer.length()-1))){
            StringBuffer s1= getLargestVovelSubsequence(s,index+1, new StringBuffer(stringBuffer).append(s[index]));
            StringBuffer s2= getLargestVovelSubsequence(s,index+1, new StringBuffer(stringBuffer));
            return s1.length()>s2.length()?s1:s2;
        }
        else if(s[index]!=nextChar(stringBuffer.charAt(stringBuffer.length()-1))){
            return getLargestVovelSubsequence(s,index+1, stringBuffer);
        }
        else return new StringBuffer();

    }

    private static char nextChar(char charAt) {
        return vovels.get(vovels.indexOf(charAt)+1);
    }

    public static boolean isValidSubsequance(String s ){
        return s.chars().map(i -> (char)i).distinct().count()==5;
    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
//
//        String s = bufferedReader.readLine();
//
//        int result = Result.longestVowelSubsequence(s);
//
//        bufferedWriter.write(String.valueOf(result));
//        bufferedWriter.newLine();
//
//        bufferedReader.close();
//        bufferedWriter.close();

        //System.out.println("Result.longestVowelSubsequence(aeiaaioooaauuaeiou) = " + Result.longestVowelSubsequence("aeiaaioooaauuaeiou"));
        //System.out.println("Result.longestVowelSubsequence(aaauuiieeou) = " + Result.longestVowelSubsequence("aaauuiieeou"));
        System.out.println("Result.longestVowelSubsequence(auaaioeoiaooaauoeuaueouuoeiiooiiaiiuioio) = " + Result.longestVowelSubsequence("auaaioeoiaooaauoeuaueouuoeiiooiiaiiuioio"));
        //System.out.println("Result.longestVowelSubsequence(aaauuiieeou) = " + Result.longestVowelSubsequence("eaaioeaieoaiueiuiaeiuueeueouoiuueeuaooooiuaiaeuaaieiauaiauuieaoeeeieeoiuaiuuuaaoieooooeioeiouuaoeaouooiauiuiioaoeeeuaoeooiueoaiuioeaeaaouiiiauiuuauoiaiaauaeooeuaiuoeeaaeoaiaoiueieeuaioieouuaaiieeaaeiioaoieoauieoueoauaieueoaeiaoaeoeuiaiauuauouoouaeaueeeioauaieiaieoaeiiueuaaoeuoiueaaiaiaouoouueoauoeieaioeeuueiaaoaiaiiaiueuauaeaieuioooiaeooeaueeoueiueeaueuuiaeuoiuiaeioeeuoaiuueuiaaueueuaeoueuaiaiiuiaouoiueuuueeaueeoaiaaouuiioeioeiaaiieaieiiieeeiaaoaeoououaooiioieuoaeaueuoeaueaoaieeeeauouaaaeiiuoiiuieuuoouuaaoiuaiaoaeeeeiauuuuoiuuoeiieuoaeaouaaooiuuuaeaeaeioeuuauaeaioiuuueuuiuieoieooeoiuioeouuuuaooeueaiooaeiieeieuauoeoaieaeiaaeeoiieiaeuouuuuououiuaueoeooaaeeuuiiaiiueoueaaauaiaieiuiiieauaaioauiuoiiiaieeuaieieiuooeaeooooeouioaooieooeaaaeeeuouiiooiaiieeeuoieeuueouiuuoioeeoiuaauuaaeaueeeiuuuueeeaaeuuoeeeuuieeueeuiaeioeaoiiiiauuoeieeioooaoaeueouiaoeouioaueoaioiuoieuoueuiuouiuaiiaeiuueaiaeuaaeouoa"));

    }
}

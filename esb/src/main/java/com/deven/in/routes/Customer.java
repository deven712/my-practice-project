package com.deven.in.routes;

import lombok.Builder;
import lombok.Singular;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Builder
public class Customer {
    private String name;
    private String department;
    @Singular
    private List<Address> addresses;
}



import com.deven.in.processors.PalindromFinder;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.Test;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@SpringBootApplication
public class ThreadsTestNPractice {

    static synchronized public void staticMethod() throws InterruptedException {
        System.out.println("Sleep in static method");
        Thread.sleep(1000);
    }

    public synchronized void instatnceMethod(){
        System.out.println("Instance method");
    }

    @Test
    public void testExecutor(){
        ExecutorService executor = Executors.newFixedThreadPool(5);//creating a pool of 5 threads
        for (int i = 0; i < 10; i++) {
            final int x= i;
            Runnable worker = () -> {
                System.out.println("This is thread #"+x);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };
            executor.execute(worker);//calling execute method of ExecutorService
            System.out.println("Released Thread #"+x);
        }
        executor.shutdown();
        while (!executor.isTerminated()) {   }

        System.out.println("Finished all threads");
    }

    @Test
    public void testStringEquality(){
        String s = "hello";
        String s1 = "people";
        String s3 = "hellopeople";
        if (s.concat(s1) == s3) {
            System.out.println("Equal Ref");
        } else {
            System.out.println("Unequal Ref");
        }
    }

    @Test
    public void testStringFrormat(){
        int a=5,b=3, n=5;
        StringBuilder template = new StringBuilder();
        Double value = new Double(a);
        for (int i =0; i<n; i++){
            value = value +  Math.pow(2,i)*b;
            if (template.length()==0) template.append(value.intValue());
            else template.append(","+ value.intValue());
        }
        System.out.println(template);
    }

    @Test
    public void testArrayList(){
        CopyOnWriteArrayList<String> lst = new CopyOnWriteArrayList<>();
        lst.add("ads");
        lst.add("hjk");
        lst.add("kdskf");
        Iterator<String> itr = lst.iterator();
        lst.remove("ads");
        while(itr.hasNext()){
            System.out.println(itr.next());
        }
        HashSet<Animal> set= new HashSet<>();
        set.add(new Animal());
        set.add(new Animal());
        System.out.println(new WorkDay("SUNDAY").equals(new WorkDay("SUNDAY")));
        for (Animal s : set){
            System.out.print(s);
        }

    }

    class Animal{
        int I =12;
        Animal(){
            I=13;
        }
        public String toString(){
            return "Animal"+I;
        }
    }
    class WorkDay{
        private String day;

        public WorkDay(String day) {
            this.day = day;
        }

        public boolean equals(Object o){
            return ((WorkDay)o).day == this.day;
        }
    }

    @Test
    public void testReps(){
        long num= 800;
        int count =0;
        for(long i =1 ; i< (num/2)+3 ; i++){
            long sum=0;
            //System.out.println("i = "+i);
            for (long j =i; j<(num/2)+3 ;j++){
                sum = sum +j;
                if (sum == num) {
                    //System.out.println("Found sum at i = " +i);
                    count++;
                    break;
                }
            }
        }
        System.out.println(count);
    }


    @Test
    public void findConsecutives(){
        long num=23232323;
        int count =0 ;
        HashMap<Long,Long> sums= new HashMap<>();
        long sum=0;
        for(long i=1; i<=num/2 + 1; i++){
            sum = sum +i;
            sums.put(i,sum);
        }
    }
    @Test
    public void testMethods(){
        method1(null);
    }
    public void method1(Object o){
        System.out.println("Print Object");
    }
    public void method1(Exception e){
        System.out.println("Print Exception");
    }

    @Test
    public void testTreemap(){
        TreeMap<String,String> dictionary = new TreeMap<>();
        dictionary.put("Elephant", "Elephnat");
        dictionary.put("Cat", "CAT");
        dictionary.put("Zebra", "Zebra");
        System.out.println(dictionary.subMap("A","J"));
    }

    @Test
    public void testThereadsExecution(){
        Thread t= new Thread(ThreadsTestNPractice::anotherMethod);
    }

    public static Integer anotherMethod(){
        return 1;
    }

    @Test
    public void testSemaphore() throws InterruptedException {
        Semaphore semaphore =new Semaphore(2);
        Thread t1 = new Thread(() -> {
            int i=0;
            while (true){
                try {
                    System.out.println("i = " + i++);
                    semaphore.acquire();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread t2 = new Thread(() -> {
            int i=0;
            while (true){
                try {
                    System.out.println("j = " + i++);
                    semaphore.release();
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t1.setDaemon(false);
        t2.setDaemon(false);
        t1.start();
        t2.start();
        t1.join();
    }

    @Test
    public void TtestCountDownLatch() throws InterruptedException {
        CountDownLatch cdl = new CountDownLatch(5);
        AtomicReference<Boolean> released = new AtomicReference<>(false);
        new Thread(() -> {
            try {
                cdl.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            released.set(true);
            System.out.println("Latch Released");
        }).start();

        new Thread(() -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while (!released.get()){
                cdl.countDown();
            }
            System.out.println("Thread 2 terminated");
        }).start();
        while (Thread.currentThread().getThreadGroup().activeCount()>1) {
            System.out.println(Thread.currentThread().getThreadGroup().activeCount());
            Thread.sleep(2000);
        }
    }

    @Test
    public void testLock() throws InterruptedException {
        Lock semaphore = new ReentrantLock();
        new Thread(() -> {
            int i=0;
            while (true){
                try {
                    System.out.println("i = " + i++);
                    semaphore.tryLock();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(() -> {
            int i=0;
            while (true){
                try {
                    System.out.println("j = " + i++);
                    semaphore.unlock();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        while (Thread.currentThread().getThreadGroup().activeCount()>1)  Thread.sleep(2000);
    }

    @Test
    public void testCyclicBarrier() throws InterruptedException {
        CyclicBarrier cb = new CyclicBarrier(5);
        for(int i=0;i<15;i++){
            int finalI = i;
            new Thread(() -> {
                try {
                    System.out.println("Thread Started "+finalI);
                    Thread.sleep(1000* finalI);
                    System.out.println("reached at CB "+ finalI);
                    cb.await();
                    System.out.println("finalI = " + finalI);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }
        while (Thread.currentThread().getThreadGroup().activeCount()>1)  Thread.sleep(2000);
    }

    @Test
    public void testExchnager() throws InterruptedException {
        Exchanger<String> exchanger= new Exchanger<>();
        new Thread(() -> {
            String s= "Red";
            while(true){
                try {
                    s = exchanger.exchange(s);
                    System.out.println("Apple is " + s);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(() -> {
            String s= "Yellow";
            while(true){
                try {
                    s = exchanger.exchange(s);
                    System.out.println("Orange is " + s);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(() -> {
            String s= "Black";
            while(true){
                try {
                    s = exchanger.exchange(s);
                    System.out.println("Grapes are " + s);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        while (Thread.currentThread().getThreadGroup().activeCount()>1)  Thread.sleep(2000);
    }

    @Test
    public void testSynchronusQueue() throws InterruptedException {
        SynchronousQueue<String> synchronousQueue = new SynchronousQueue<>();
        for (int i = 0; i < 4; i++) {
            int finalI = i;
            new Thread(() -> {
                String name = "Producer"+ finalI;
                Random random = new Random();
                for(int j =0; j<3 ;j++){
                    try {
                        String element = new String(name+random.nextInt(10));
                        System.out.println( name + " Produced " + element);
                        synchronousQueue.put(element);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
        new Thread(() -> {
            String name = "Consumer";
            Random random = new Random();
            while(true){
                try {
                    Thread.sleep(500);
                    System.out.println( name + " consumed " + synchronousQueue.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        while (Thread.currentThread().getThreadGroup().activeCount()>1)  {
            Thread.sleep(2000);
        }
    }

    @Test
    public void testcode() {
        byte a[] = {65, 66, 67, 68, 69, 70};

        byte b[] = {71, 72, 73, 74, 75, 76};

        System.arraycopy(a, 0, b, 0, a.length);

        System.out.print(new String(a) + " " + new String(b));

    }

    @Test
    public void test1(){
        Object i = new ArrayList().iterator();
        System.out.print((i instanceof List)+",");
        System.out.print((i instanceof Iterator)+",");
        System.out.print(i instanceof ListIterator);
    }

    @Test
    public void testpalindromFunctionality(){
        PalindromFinder pf= new PalindromFinder();
        pf.findPalindroms("lasksdhqwerrewqjshdkjhsqwertyuioppoiuytrzkljkljasd",false);
        while (pf.locks.get()!=0) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("pf.getPalindroms() = " + pf.getPalindroms());
        System.out.println(pf.getPalindroms().stream().max(Comparator.comparing(String::length)).orElse(""));
    }

    @Test
    public void testCompletableFutureThenCombine(){
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> "String")
                .thenCombine(CompletableFuture.supplyAsync(() -> 5),(s, integer) -> s+integer);
    }

}

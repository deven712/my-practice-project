import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.Setter;
import lombok.Synchronized;
import lombok.Value;
import org.hibernate.validator.internal.util.stereotypes.Immutable;

@Data
@AllArgsConstructor
public class Bicycle {
    private String brand;
    private Integer frameSize;
}

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class JavaCollectionTest {
    public List getStringList(){
        List<String> list= new ArrayList<>();
        list.add("Alphabet");
        list.add("Bubble");
        list.add("Cycle");
        list.add("Dog");
        list.add("Lemon");
        list.add("Master");
        list.add("Never");
        list.add("XMas");
        list.add("Year");
        list.add("Zebra");
        return list;
    }

    public List<Bicycle> getBicycleList(){
        ArrayList<Bicycle> list = new ArrayList<>();
        Random random= new Random();
        for(int i=0;i<10;i++){
            Bicycle obj =new Bicycle(Long.toString(random.nextLong()),random.nextInt(10));
            list.add(obj);
        }
        return list;
    }

    public List<Integer> getIntegerList(){
        ArrayList<Integer> list = new ArrayList<>();
        Random random= new Random();
        for(int i=0;i<10;i++){
            list.add(random.nextInt(10));
        }
        return list;
    }

    @Test
    public void testIteratorDelete(){
        List<String> list = this.getStringList();
        Iterator<String> itr = list.iterator();
        String tmp;
        while(itr.hasNext()){
            tmp =itr.next();
            if ("Cycle".equals(tmp))itr.remove();
        }
//        while(itr2.hasNext()){
//            tmp =itr2.next();
//            if ("Dog".equals(tmp))itr2.remove();
//        }
        System.out.println(list);
    }


    //Arraylist with new references for elements.
    @Test
    public void testListCopy(){
        ArrayList<String> list = (ArrayList<String>) this.getStringList();
        ArrayList<String> copyList = (ArrayList<String>) list.clone();
        Iterator<String> itr = list.iterator();
        String tmp;
        while(itr.hasNext()){
            tmp =itr.next();
            if ("Cycle".equals(tmp))itr.remove();
        }
        System.out.println(copyList);
        System.out.println(list);
    }

    //List with new References but Objects are same on that references
    @Test
    public void testObjListCopy(){
        ArrayList<Bicycle> list = (ArrayList<Bicycle>) this. getBicycleList();
        ArrayList<Bicycle> copyList = (ArrayList<Bicycle>) list.clone();
        Iterator<Bicycle> itr = list.iterator();
        int i=0;
        while(itr.hasNext()){
            Bicycle obj = itr.next();
            if (i==3)obj.setBrand("Hero");i++;
        }
        System.out.println(list);
        System.out.println(copyList);
    }

    @Test
    public void testArrayListAdd(){
        ArrayList<Integer> intList =  new ArrayList<>();
        intList.add(intList.size() , 4);
        System.out.println(intList);
        intList = (ArrayList<Integer>) getIntegerList();
        intList.add(intList.size() , 4);
        System.out.println(intList);
    }

    @Test
    public void testNaturalOrder(){
//        TreeSet<String> set1= new TreeSet<>(this.getStringList());
        System.out.println(new TreeSet<>(this.getStringList()));
        System.out.println(new TreeSet<>(this.getIntegerList()));
    }

    @Test
    public void testPriorityQueue(){
        Queue<ComparablePerson> pq = new PriorityQueue<>();
        pq.add(new ComparablePerson(1, "John"));
        pq.add(new ComparablePerson(4, "Ken"));
        pq.add(new ComparablePerson(2, "Richard"));
        pq.add(new ComparablePerson(3, "Donna"));
        pq.add(new ComparablePerson(4, "Adam"));
        System.out.println("Priority queue: " + pq);
        while (pq.peek() != null) {
            System.out.println("Head Element: " + pq.peek());
            pq.remove();
            System.out.println("Removed one element from Queue");
            System.out.println("Priority queue: " + pq);
        }
    }

    @Test
    public void testDequeImplementation(){
        LinkedList<String> list = new LinkedList<>(this.getStringList());
        list.add("Hero");
        list.addFirst("New");
        list.addLast("Member");
//        list.add(20,"added");
        System.out.println(list);
    }

    @Test
    public void testStringOccurance(){
        List<Integer> intList = this.getIntegerList();
        LinkedHashMap<String, String> LHM= new LinkedHashMap<>();

//        Object o= intList.stream().collect(Collectors.groupingBy(integer -> integer,Collectors.counting()));
//        System.out.println("o = " + o);
//        int sum = intList.stream().reduce()
    }

    @Test
    public void testStreamPropagation(){
        List<Integer> integerList= this.getIntegerList();
        System.out.println("integerList = " + integerList);
        integerList.stream().filter(integer -> {
            System.out.println(" Filter integer = " + integer);
            return integer%2 ==0;
        }).flatMap(integer -> {
            System.out.println("Flat Map integer = " + integer);
            return Arrays.stream(new Integer[]{integer-1, integer, integer+1});
        }).distinct().map(integer -> {
            System.out.println("Map integer = " + integer);
            return integer;
        }).collect(Collectors.toList());
    }
}

class ComparablePerson implements Comparable<ComparablePerson> {
    Integer id;
    String name;

    public ComparablePerson(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(ComparablePerson cp) {
        int cpId = cp.getId();
        String cpName = cp.getName();
        if(!this.id.equals(cp.id)) return this.id.compareTo(cp.id);
        else if(! this.name.equals(cp.name))return this.getName().compareTo(cpName);
// Should not reach here
        return 0;
    }

    @Override
    public String toString() {
        return "ComparablePerson{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import lombok.Data;
import lombok.Setter;
import lombok.Value;
import org.assertj.core.internal.Integers;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class JavaAppTest {

    @Test
    public void testListsSorting(){
        List<Integer> intList = Arrays.asList(1,3,6,2,3,7,8,5,9);
        System.out.println(intList);
        List<Integer> sortedIntList= intList.stream().sorted().collect(Collectors.toList());
        System.out.println(sortedIntList);
        List revSortedIntList = intList.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        System.out.println(revSortedIntList);
        System.out.println(intList.get(0));
        System.out.println(sortedIntList.get(0));
        System.out.println(revSortedIntList.get(0));
    }

    @Test
    public void testStringListsSorting(){
        List<String> intList = Arrays.asList("A","a","H","h","L","m","z","Z");
        System.out.println(intList);
        List<String> sortedIntList= intList.stream().sorted(String::compareToIgnoreCase).collect(Collectors.toList());
        System.out.println(sortedIntList);
        List revSortedIntList = intList.stream().sorted(Comparator.comparing(String::toUpperCase, Comparator.reverseOrder())).collect(Collectors.toList());
        System.out.println(revSortedIntList);
        System.out.println(intList.get(0));
        System.out.println(sortedIntList.get(0));
        System.out.println(revSortedIntList.get(0));
        Function<String,String> f1= s -> s.toUpperCase();
        Function<String, String> f2= String::toUpperCase;
        Function<String,Integer> f3= s -> Integer.parseInt(s);
        Function<String,Integer> f4= Integer::parseInt;
    }

    @Test
    public void testArbClassComparator(){
        ArbClass obj1= new ArbClass(25, "Deven", 450);
        ArbClass obj2= new ArbClass(25, "Rahul", 500);
        ArbClass obj3= new ArbClass(30, "rohit", 320);
        ArbClass obj4= new ArbClass(30, "dinesh", 450);
        List<ArbClass> arbClassList = Arrays.asList(obj1,obj2,obj3,obj4);
        Comparator ageComparator= Comparator.comparing(ArbClass::getAge);
        Comparator nameComparator= Comparator.comparing(ArbClass::getName);
        Function<ArbClass,String> getName= ArbClass::getName;
        Function<String,String> doCapital = String::toUpperCase;
        Function<ArbClass,String> getCapitalName1= getName.andThen(doCapital);
        Function<ArbClass,String> getCapitalName2= doCapital.compose(getName);
        //Comparator<ArbClass> caseInsensitiveComparator = Comparator.comparing(getCapitalName1);
        Comparator<ArbClass> caseInsensitiveComparator = Comparator.comparing(getCapitalName2);
        Comparator reverseCaseInsensitiveComparator = Comparator.comparing(getCapitalName1,Comparator.reverseOrder());
        //Comparator reverseCaseInsensitiveComparator = caseInsensitiveComparator.reversed();
        List<ArbClass> sortedList= (List<ArbClass>) arbClassList.stream().sorted(ageComparator.thenComparing(caseInsensitiveComparator)).collect(Collectors.toList());
        //List<ArbClass> sortedList= (List<ArbClass>) arbClassList.stream().sorted(Comparator.comparing(ArbClass::getAge).thenComparing(getName));
        System.out.println(sortedList);
        TreeSet<ArbClass> treeSet = new TreeSet<>(ageComparator);
        arbClassList.stream().forEach(arbClass -> {
            System.out.println("arbClass = " + arbClass);
            treeSet.add(arbClass);
        });
        System.out.println("treeSet = " + treeSet);
        int i=0;
        TreeMap<ArbClass, Integer> treeMap = new TreeMap<>(ageComparator);
        for(ArbClass obj : arbClassList){
            System.out.println("obj = " + obj);
            System.out.println("treeMap.put(obj,i++) = " + treeMap.put(obj, i++));
        }
        System.out.println("treeMap = " + treeMap);
//        treeSet.addAll(arbClassList);
//        Iterator<ArbClass> lI= treeSet.descendingIterator();
//        Map map = sortedList.stream().collect(Collectors.groupingBy(ArbClass::getAge));

    }
    @Test
    public void testImmutableCollection(){
        List<Integer> integers= Arrays.asList(1,2,3,4,5,6,7);
        System.out.println(integers);
        integers.set(1,5);
        System.out.println(integers);
    }

    @Test
    public void testCompareLength(){
        SortedSet<String> names = new TreeSet<>(Comparator.comparing(String::length).thenComparing(String::compareTo));
        names.add("Ken");
        names.add("Lo");
        names.add("Ellen");
        names.add("Jen"); // A duplicate that is ignored
// Print the unique sorted names
        names.forEach(System.out::println);
    }

    @Test
    public void testSubsetOperations(){
        SortedSet<String> names = new TreeSet<>();
        names.add("John");
        names.add("Adam");
        names.add("Eve");
        names.add("Donna");
        System.out.println("Sorted Set: " + names);
        System.out.println("First: " + names.first());
        System.out.println("Last: " + names.last());
        SortedSet ssBeforeDonna = names.headSet("Donna");
        System.out.println("Head Set Before Donna: " + ssBeforeDonna);
        SortedSet ssBetwenDonnaAndJohn = names.subSet("Donna", "John");
        System.out.println("Subset between Donna and John (exclusive): "
                + ssBetwenDonnaAndJohn);
        SortedSet ssBetwenDonnaAndJohn2 = names.subSet("Donna", "John" + "\0");
        System.out.println("Subset between Donna and John (Inclusive): "
                + ssBetwenDonnaAndJohn2);
        SortedSet ssDonnaAndAfter = names.tailSet("Donna");
        System.out.println("Subset from Donna onwards: " + ssDonnaAndAfter);
    }

    public void changePrim(int a){
        a=5;
    }
    public void changeString(String s){
        s="Hello";
    }
    public void changeObject(Bicycle obj){
        obj.setFrameSize(15);
    }
    public void changeArray(int[] intArray){
        intArray[intArray.length -1] = 15;
    }

    @Test
    public void testObjReference(){
        System.out.println("hello".compareTo("zebra"));
    }

    @Test
    public void testHash(){
        String s= "Hello";
        String s2= new String(s);
        System.out.println(s2.hashCode()==s.hashCode());
        System.out.println(s2==s);
        System.out.println("s2.equals(s1) = " + s2.equals(s));
    }

    @Test
    public void testNumberOfOccurrence(){
        Integer[] i =new Integer[10];
        Integer[] j= new Integer[100];
        List<A> aList= new ArrayList<>();
        List<B> blist = new ArrayList<>();
        List<A> clist = new ArrayList();
        List<? extends Object> anyList = new ArrayList<>();
        List dList = blist;
        List<B> eList = blist;
//        aList.add(new B());
    }

    @Test
    public void testGenerics(){
    }

    // java.io.InvalidClassException: C; no valid constructor
//    @Test(expected = InvalidClassException.class)
    @Test
    public void testSerializable() throws IOException, ClassNotFoundException {
        C c= new C(5);
        System.out.println("c = " + c);
        ByteArrayOutputStream baos= new ByteArrayOutputStream();
        ObjectOutputStream oos= new ObjectOutputStream(baos);
        oos.writeObject(c);
        oos.flush();
        byte[] stream = baos.toByteArray();
        ByteArrayInputStream bais = new ByteArrayInputStream(stream);
        ObjectInputStream ois = new ObjectInputStream(bais);
        C obj = (C) ois.readObject();
        System.out.println("obj = " + obj);
    }

    @Test
    public void testCloneable() throws CloneNotSupportedException {
        C c = new C(4);
        C c1= (C) c.clone();
        System.out.println("c = " + c);
        System.out.println("c1 = " + c1);
    }

    @Test
    public void listRetentionTest(){
        List<String> list = new ArrayList<>();
        list.add("Hello");
        list.add("Hello");
        list.add("Hello");
        list.add("Friend");
        list.add("Hi");
        list.retainAll(Collections.singleton("Hello"));
        System.out.println("list = " + list);

    }


}

@Setter
@Value
class ArbClass{
    Integer age;
    String Name;
    Integer Salary;
}


class A implements Serializable{
    private int i;

    public A(int i) {
        this.i = i;
    }
}
class B extends A{

    public B(int i) {
        super(i);
    }
    public static void staticMethod(){

    }
    public void method1() throws Throwable {

    }
}
class C extends B implements Cloneable{
    public C(int i) {
        super(i);
    }
    public static void staticMethod(){

    }

    public void method1() throws Throwable {
        super.staticMethod();
        this.staticMethod();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

@Data
class GeneriricTest<T>{
    B b;
    T obj;
    T[] array ;
    List<T> list;
    public <X extends Runnable, Y > void method1(T t, X x, Y y, GeneriricTest<? extends Runnable> z){

    }
}

import org.junit.Test;

public class MiscTestTopics {

    @Test
    public void testStringBuffer(){
        StringBuffer sb1= new StringBuffer();
        sb1.append("Hello");
        StringBuffer sb2 = new StringBuffer(sb1);
        sb2.append(" friend");
        System.out.println("sb1==sb2 = " + (sb1==sb2));
        System.out.println("sb1 = " + sb1);
        System.out.println("sb2 = " + sb2);

    }
}

